FROM docker.io/rust as builder
RUN apt install git \
 && git clone https://github.com/matrix-org/rust-synapse-compress-state.git \
 && cd rust-synapse-compress-state/synapse_auto_compressor \
 && cargo build --release

FROM docker.io/debian:bullseye-slim
COPY --from=builder /rust-synapse-compress-state/target/release/synapse_auto_compressor /usr/local/bin
ENTRYPOINT /usr/local/bin/synapse_auto_compressor
